# Introduccion Al Análisis de Datos (Coursera)
link al curso https://www.coursera.org/programs/fundamentos-de-la-ciencia-de-datos-2laqv?currentTab=CATALOG&productId=098ICQ5KEeuJGwrcf6g9pw&productType=course&showMiniModal=true 

## Temas
1.  El ecosistema de Datos modernos y el rol del análisis de datos  

## El ecosistema de Datos modernos y el rol del análisis de datos

### Ecosistemas de datos modernos 
Los ecosistemas de datos modernos incluyen una red completa de entidads  interconectados, idependientes y en constante evolucion. Datos obtenidos de diferentes fuentes. Diferentes tipos de análisis y habilidades para generar perspectivas. Colaboradores que actuan sobre las perspectivas generadas. Herramientas, aplicaciones e infraestructura para almacenar, procesar y desiminar datos como sea necesario

Los datos están disponibles en conjuntos estructurados y no estructurados de textos, imagenes, videos, secuencias, conversaciones, redes sociales, internet de las cosas, eventos en tiempo real, fuentas de datos de profesionales y bases de datos. 

*Al tener tantas fuentes de información, el paso uno es siempre crear un repositorio local, en esta etapa te enfrentas a la fiabiidad, seguridad e integridad de los datos.*

*Una vez se han juntado todos los datos en una base de datos es momento de organizarla, limpiarla y optimizar el acceso y comodidad del usuario. Así como cumplir con los estandares de la organización. El reto en esta etapa es la administracion de los datos y trabajar con repositorios de datos que proveen alta disponibilidad, accesibilidad y seguridad.*

*Por último tenemos a los usuarios que pueden ser directivos de negocios, aplicaciones, programadores y casos de uso de ciencias de datos. Todo esto puesto en un repositorio de empresa. En este punto los retos pueden ser crear interfaces, API's o aplicaciones que pongan toda la información en manos de los usuarios finales para su uso especifico*

*Los cientificos de datos pueden crear modelos predictivos de machine learning de informacion pasada.*

### Protagonistas clave en el ecosistema de datos.
*Intependientemente del uso que quieran dar a los datos, los empresarios y directivos saben que de ellos se obtiene una ventaja competitiva, para obtener valor de los datos requieres un conjunto basto de habilidades y personas participando en diferentes roles.*

Data Engineer. Es quien desarrolla y mantiene la estructura de los datos, mantiene los datos disponibles para operaciones de negocio y analisis. Trabaja en el ecosistema de datos extrayendo y organizando datos de diferentes fuentes. Limpiando, transformando y preparando datos. Diseñar, almacenar y administrar datos en repositorios. Ellos permiten que los datos sean accesibles para los colaboradores como analistas de datos y cientificos de datos. Deben tener habilidades en desarrollo de bases de datos relacionales y no relacionales.

Data Analyst. Convierten los datos en información plana para que las organizaciones tomen deciciones. Inspecciona y limpia los datos para obtener perspectivas. Identifica coorrelacione, encuentra patrones y aplica metodos estadisticos. Visualiza datos para intrepretarlos y presentarlos hallazgos. Son los que responden las preguntas subjetivas de las organizaciones acerca de los usuarios.

*Data Scientist. Analiza datos para obtener informacion procesable. Crea modelos predictivos usando machine learning y deep learning*. Responde a las preguntas objetivas en terminos de números, como:
    1. Cuantos seguidores es probable que obtenga el siguiente mes. 
    2. Cuantos clientes podría perder ante la compentencia el siguiente trimestre. 
    3. Es financieramente inusual está operacion de estos clientes?
Debe detener habilidades en matematicas, estadistica, lenguajes de programacion y construccion de modelos de datos, asi como conocimiento del dominio (entendimiento especializado de la ciencia de datos)

*Data scientist use data Analytics and Data engineering para predecir el futuro a partir del pasado. Business Analysts and Business intelligence Analysts  usan estas perspectivas y predicciones para crear deciciones que beneficien y hagan crecer al organismo.*

### Definición de Analisis de datos
*Es el proceso de recolectar, limpiar, analizar  y minar datos, interpretando resultados y reportando los descubrimientos. El analisis de datos ayuda a los negocios a entender su desempeño pasado e ilustra sobre la toma de deciciones en acciones futuras.* 

*El analisis de datos permite validar el curso de una accion antes de tomar la decicion de hacerla, ahorrando recursos y tiempo. Además asegurando el exito de una decicion sin tomarla.*

*Analisis descriptivo. Ayuda a responder la pregunta. Que pasó? Usando información pasada. Creando perspectivas de eventos pasados*
*Analisis Diagnostico. Ayuda a responder ¿Porque paso?. Toma las perspectivas de los eventos pasados para obtener una razón profunda de porque ocurrió algo.*
*Analisis Predictos. Ayuda a responder la pregunta. Que ocurrirá?. Utiliza datos historicos y tendicias de los mismos. Debe quedar claro que todas las predicciones son probables por naturaleza.* 
*Analisis prescriptivo. Ayuda a responder la pregunta. Que debería hacer al respecto?. Analiza diferentes casos que podrían ocurrir a partir de una decición, usando datos historicos.*

<b>Proceso para realizar un analis de datos.</b>
1. Entender el problema y el resultado deseado.
2. Establecer que será medido y en base a qué metricas.
3. Identificar los datos que requieres, fuentes y herramientas para obtenerlas.
4. Limpiar datos de posibles errores que provoquen perder calidad.
5. Minar y Analizar los datos; mirandolos desde distintas perspectivas, identificar coorrelaciones y encontrar patrones.
6. Interpretar los resultados. Evaluando si tu analisis es defendible ante argumentos.
7. Presentar resultados para impactar en las deciciones.


Responsabilidades de un Analista de datos.
Adquirir datos de diferentes fuentes.
Crear consultas para extraer datos de bases de datos.
Filtrar datos relevantes.
Usar tecnicas estadisticas para identificar patrones
Analizar datos y patrones.
Crear informes y documentar.

